using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public List<UsersDTO> Users { get; set; }
    }
}