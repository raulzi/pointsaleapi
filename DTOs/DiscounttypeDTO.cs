using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class DiscounttypeDTO
    {
        public int Iddiscounttype { get; set; }
        public string Description { get; set; }

        public List<ArticleDTO> Article { get; set; }
    }
}