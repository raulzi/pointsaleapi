using System;

namespace PointSaleAPI.DTOs
{
    public class PaymentDTO
    {
        public int Idpayment { get; set; }
        public DateTime Paymentdate { get; set; }
        public decimal Amount { get; set; }
        public int Iduser { get; set; }
        public string Comment { get; set; }
        public int Idsalenote { get; set; }

        public SalenoteDTO IdsalenoteNavigation { get; set; }
        public UsersDTO IduserNavigation { get; set; }
    }
}