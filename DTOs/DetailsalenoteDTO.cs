namespace PointSaleAPI.DTOs
{
    public class DetailsalenoteDTO
    {
        public int Idsalenote { get; set; }
        public int Idarticle { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public ArticleDTO IdarticleNavigation { get; set; }
        public SalenoteDTO IdsalenoteNavigation { get; set; }
    }
}