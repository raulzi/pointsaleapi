using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class ColorDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public virtual List<ArticleDTO> Article { get; set; }
    }
}