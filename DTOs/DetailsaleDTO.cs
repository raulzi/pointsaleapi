namespace PointSaleAPI.DTOs
{
    public class DetailsaleDTO
    {
        public int Idsale { get; set; }
        public int Idarticle { get; set; }
        public decimal Price { get; set; }
        public decimal Additionaldiscount { get; set; }
        public int Quantity { get; set; }

        public ArticleDTO IdarticleNavigation { get; set; }
        public SaleDTO IdsaleNavigation { get; set; }
    }
}