using System;
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class ArticleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
        public int Stock { get; set; }
        public DateTime Registrationdate { get; set; }
        public int Idnumeration { get; set; }
        public int Idcolor { get; set; }
        public int Idgender { get; set; }
        public int Idsupplier { get; set; }
        public int? Iddiscounttype { get; set; }
        public int Iduser { get; set; }

        public ColorDTO IdcolorNavigation { get; set; }
        public DiscounttypeDTO IddiscounttypeNavigation { get; set; }
        public GenderDTO IdgenderNavigation { get; set; }
        public NumerationDTO IdnumerationNavigation { get; set; }
        public SupplierDTO IdsupplierNavigation { get; set; }
        public UsersDTO IduserNavigation { get; set; }
        public List<DetailsaleDTO> Detailsale { get; set; }
        public List<DetailsalenoteDTO> Detailsalenote { get; set; }
    }
}