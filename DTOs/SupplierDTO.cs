
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class SupplierDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }
        public string Ownername { get; set; }

        public List<ArticleDTO> Article { get; set; }
    }
}