using System;
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class SaleDTO
    {
        public int Idsale { get; set; }
        public DateTime Saledate { get; set; }
        public decimal Amount { get; set; }
        public int Iduser { get; set; }

        public UsersDTO IduserNavigation { get; set; }
        public List<DetailcashoutDTO> Detailcashout { get; set; }
        public List<DetailsaleDTO> Detailsale { get; set; }
    }
}