using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class GenderDTO
    {
        public int Idgender { get; set; }
        public string Description { get; set; }

        public List<ArticleDTO> Article { get; set; }
    }
}