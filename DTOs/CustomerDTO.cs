using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class CustomerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }

        public virtual List<SalenoteDTO> Salenote { get; set; }
    }
}