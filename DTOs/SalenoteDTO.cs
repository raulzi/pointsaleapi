
using System;
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class SalenoteDTO
    {
        public int Idsalenote { get; set; }
        public int Idcustomer { get; set; }
        public DateTime Saledate { get; set; }
        public decimal Amount { get; set; }
        public bool Ispaid { get; set; }
        public DateTime? Paymentdate { get; set; }
        public int Iduser { get; set; }

        public CustomerDTO IdcustomerNavigation { get; set; }
        public UsersDTO IduserNavigation { get; set; }
        public List<DetailsalenoteDTO> Detailsalenote { get; set; }
        public List<PaymentDTO> Payment { get; set; }
    }
}