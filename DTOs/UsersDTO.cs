
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class UsersDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public bool Active { get; set; }
        public int Idrole { get; set; }

        public RoleDTO IdroleNavigation { get; set; }
        public List<ArticleDTO> Article { get; set; }
        public List<CashoutDTO> Cashout { get; set; }
        public List<PaymentDTO> Payment { get; set; }
        public List<SaleDTO> Sale { get; set; }
        public List<SalenoteDTO> Salenote { get; set; }
    }
}