using System;
using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class CashoutDTO
    {
        public int Idcashout { get; set; }
        public decimal Amount { get; set; }
        public decimal Missing { get; set; }
        public DateTime Cashoutdate { get; set; }
        public DateTime Sincedate { get; set; }
        public DateTime Todate { get; set; }
        public int Iduser { get; set; }

        public  UsersDTO IduserNavigation { get; set; }
        public  List<DetailcashoutDTO> Detailcashout { get; set; }
    }
}