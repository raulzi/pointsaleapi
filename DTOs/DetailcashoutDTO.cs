namespace PointSaleAPI.DTOs
{
    public class DetailcashoutDTO
    {
        public int Idcashout { get; set; }
        public int Idsale { get; set; }

        public CashoutDTO IdcashoutNavigation { get; set; }
        public SaleDTO IdsaleNavigation { get; set; }
    }
}