using System.Collections.Generic;

namespace PointSaleAPI.DTOs
{
    public class NumerationDTO
    {
        public int Id { get; set; }
        public decimal Lowerrange { get; set; }
        public decimal Toprange { get; set; }

        public List<ArticleDTO> Article { get; set; }
    }
}