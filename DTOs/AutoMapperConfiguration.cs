using AutoMapper;
using PointSaleAPI.Models;

namespace PointSaleAPI.DTOs
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {

                cfg.CreateMap<Article, ArticleDTO>()
                .ReverseMap();

                cfg.CreateMap<Cashout, CashoutDTO>()
                .ReverseMap();

                cfg.CreateMap<Color, ColorDTO>()
                .ReverseMap();
                
                cfg.CreateMap<Customer, CustomerDTO>()
                .ReverseMap();

                cfg.CreateMap<Detailcashout, DetailcashoutDTO>()
                .ForMember(x => x.IdcashoutNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Detailsale, DetailsaleDTO>()
                .ForMember(x => x.IdsaleNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Detailsalenote, DetailsalenoteDTO>()
                .ForMember(x => x.IdsalenoteNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Discounttype, DiscounttypeDTO>()
                .ReverseMap();

                cfg.CreateMap<Gender, GenderDTO>()
                .ReverseMap();

                cfg.CreateMap<Numeration, NumerationDTO>()
                .ReverseMap();

                cfg.CreateMap<Payment, PaymentDTO>()
                .ReverseMap();

                cfg.CreateMap<Role, RoleDTO>()
                .ReverseMap();

                cfg.CreateMap<Sale, SaleDTO>()
                .ForMember(x => x.IduserNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Salenote, SalenoteDTO>()
                .ForMember(x => x.IdcustomerNavigation, o => o.Ignore())
                .ReverseMap();

                cfg.CreateMap<Supplier, SupplierDTO>()
                .ReverseMap();

                cfg.CreateMap<Users, UsersDTO>()
                .ForMember(x => x.IdroleNavigation, o => o.Ignore())
                .ReverseMap();

            });
        }
    }
}