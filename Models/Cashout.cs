﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Cashout
    {
        public Cashout()
        {
            Detailcashout = new HashSet<Detailcashout>();
        }

        public int Idcashout { get; set; }
        public decimal Amount { get; set; }
        public decimal Missing { get; set; }
        public DateTime Cashoutdate { get; set; }
        public DateTime Sincedate { get; set; }
        public DateTime Todate { get; set; }
        public int Iduser { get; set; }

        public virtual Users IduserNavigation { get; set; }
        public virtual ICollection<Detailcashout> Detailcashout { get; set; }
    }
}
