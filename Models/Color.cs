﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Color
    {
        public Color()
        {
            Article = new HashSet<Article>();
        }

        [Column("Idcolor")]
        public int Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
