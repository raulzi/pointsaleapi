﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Salenote = new HashSet<Salenote>();
        }

        [Column("Idcustomer")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }

        public virtual ICollection<Salenote> Salenote { get; set; }
    }
}
