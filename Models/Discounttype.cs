﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Discounttype
    {
        public Discounttype()
        {
            Article = new HashSet<Article>();
        }

        public int Iddiscounttype { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
