﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Detailcashout
    {
        public int Idcashout { get; set; }
        public int Idsale { get; set; }

        public virtual Cashout IdcashoutNavigation { get; set; }
        public virtual Sale IdsaleNavigation { get; set; }
    }
}
