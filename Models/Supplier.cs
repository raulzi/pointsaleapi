﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            Article = new HashSet<Article>();
        }

        [Column("Idsupplier")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }
        public string Ownername { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
