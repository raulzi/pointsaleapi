﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Payment
    {
        public int Idpayment { get; set; }
        public DateTime Paymentdate { get; set; }
        public decimal Amount { get; set; }
        public int Iduser { get; set; }
        public string Comment { get; set; }
        public int Idsalenote { get; set; }

        public virtual Salenote IdsalenoteNavigation { get; set; }
        public virtual Users IduserNavigation { get; set; }
    }
}
