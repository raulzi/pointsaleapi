﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Users
    {
        public Users()
        {
            Article = new HashSet<Article>();
            Cashout = new HashSet<Cashout>();
            Payment = new HashSet<Payment>();
            Sale = new HashSet<Sale>();
            Salenote = new HashSet<Salenote>();
        }

        [Column("Iduser")]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public bool Active { get; set; }
        public int Idrole { get; set; }

        public virtual Role IdroleNavigation { get; set; }
        public virtual ICollection<Article> Article { get; set; }
        public virtual ICollection<Cashout> Cashout { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
        public virtual ICollection<Sale> Sale { get; set; }
        public virtual ICollection<Salenote> Salenote { get; set; }
    }
}
