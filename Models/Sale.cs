﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Sale
    {
        public Sale()
        {
            Detailcashout = new HashSet<Detailcashout>();
            Detailsale = new HashSet<Detailsale>();
        }

        public int Idsale { get; set; }
        public DateTime Saledate { get; set; }
        public decimal Amount { get; set; }
        public int Iduser { get; set; }

        public virtual Users IduserNavigation { get; set; }
        public virtual ICollection<Detailcashout> Detailcashout { get; set; }
        public virtual ICollection<Detailsale> Detailsale { get; set; }
    }
}
