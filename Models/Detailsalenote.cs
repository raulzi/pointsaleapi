﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Detailsalenote
    {
        public int Idsalenote { get; set; }
        public int Idarticle { get; set; }
        public string Code { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public virtual Article IdarticleNavigation { get; set; }
        public virtual Salenote IdsalenoteNavigation { get; set; }
    }
}
