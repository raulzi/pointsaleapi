﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Salenote
    {
        public Salenote()
        {
            Detailsalenote = new HashSet<Detailsalenote>();
            Payment = new HashSet<Payment>();
        }

        public int Idsalenote { get; set; }
        public int Idcustomer { get; set; }
        public DateTime Saledate { get; set; }
        public decimal Amount { get; set; }
        public bool Ispaid { get; set; }
        public DateTime? Paymentdate { get; set; }
        public int Iduser { get; set; }

        public virtual Customer IdcustomerNavigation { get; set; }
        public virtual Users IduserNavigation { get; set; }
        public virtual ICollection<Detailsalenote> Detailsalenote { get; set; }
        public virtual ICollection<Payment> Payment { get; set; }
    }
}
