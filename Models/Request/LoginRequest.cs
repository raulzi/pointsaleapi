using System.ComponentModel.DataAnnotations;

namespace PointSaleAPI.Models.Request
{
    public class LoginRequest
    {
        [Required]
        public string username { get; set; }

        [Required]
        public string password { get; set; }
    }
}