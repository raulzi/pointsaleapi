﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Detailsale
    {
        public int Idsale { get; set; }
        public int Idarticle { get; set; }
        public decimal Price { get; set; }
        public decimal Additionaldiscount { get; set; }
        public int Quantity { get; set; }

        public virtual Article IdarticleNavigation { get; set; }
        public virtual Sale IdsaleNavigation { get; set; }
    }
}
