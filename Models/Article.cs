﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Article
    {
        public Article()
        {
            Detailsale = new HashSet<Detailsale>();
            Detailsalenote = new HashSet<Detailsalenote>();
        }

        [Column("Idarticle")]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
        public int Stock { get; set; }
        public DateTime Registrationdate { get; set; }
        public int Idnumeration { get; set; }
        public int Idcolor { get; set; }
        public int Idgender { get; set; }
        public int Idsupplier { get; set; }
        public int? Iddiscounttype { get; set; }
        public int Iduser { get; set; }

        public virtual Color IdcolorNavigation { get; set; }
        public virtual Discounttype IddiscounttypeNavigation { get; set; }
        public virtual Gender IdgenderNavigation { get; set; }
        public virtual Numeration IdnumerationNavigation { get; set; }
        public virtual Supplier IdsupplierNavigation { get; set; }
        public virtual Users IduserNavigation { get; set; }
        public virtual ICollection<Detailsale> Detailsale { get; set; }
        public virtual ICollection<Detailsalenote> Detailsalenote { get; set; }
    }
}
