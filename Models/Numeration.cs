﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PointSaleAPI.Models
{
    public partial class Numeration
    {
        public Numeration()
        {
            Article = new HashSet<Article>();
        }

        [Column("Idnumeration")]
        public int Id { get; set; }
        public decimal Lowerrange { get; set; }
        public decimal Toprange { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
