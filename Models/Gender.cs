﻿using System;
using System.Collections.Generic;

namespace PointSaleAPI.Models
{
    public partial class Gender
    {
        public Gender()
        {
            Article = new HashSet<Article>();
        }

        public int Idgender { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
