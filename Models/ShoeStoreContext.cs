﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PointSaleAPI.Models
{
    public partial class ShoeStoreContext : DbContext
    {
        public ShoeStoreContext()
        {
        }

        public ShoeStoreContext(DbContextOptions<ShoeStoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<Cashout> Cashout { get; set; }
        public virtual DbSet<Color> Color { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Detailcashout> Detailcashout { get; set; }
        public virtual DbSet<Detailsale> Detailsale { get; set; }
        public virtual DbSet<Detailsalenote> Detailsalenote { get; set; }
        public virtual DbSet<Discounttype> Discounttype { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<Numeration> Numeration { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Sale> Sale { get; set; }
        public virtual DbSet<Salenote> Salenote { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Article>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("article_pkey");

                entity.ToTable("article");

                entity.Property(e => e.Id).HasColumnName("idarticle");

                entity.Property(e => e.Brand)
                    .IsRequired()
                    .HasColumnName("brand");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("money");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("money");

                entity.Property(e => e.Idcolor).HasColumnName("idcolor");

                entity.Property(e => e.Iddiscounttype).HasColumnName("iddiscounttype");

                entity.Property(e => e.Idgender).HasColumnName("idgender");

                entity.Property(e => e.Idnumeration).HasColumnName("idnumeration");

                entity.Property(e => e.Idsupplier).HasColumnName("idsupplier");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");

                entity.Property(e => e.Registrationdate)
                    .HasColumnName("registrationdate")
                    .HasColumnType("date");

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.HasOne(d => d.IdcolorNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Idcolor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_idcolor_fkey");

                entity.HasOne(d => d.IddiscounttypeNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Iddiscounttype)
                    .HasConstraintName("article_iddiscounttype_fkey");

                entity.HasOne(d => d.IdgenderNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Idgender)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_idgender_fkey");

                entity.HasOne(d => d.IdnumerationNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Idnumeration)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_idnumeration_fkey");

                entity.HasOne(d => d.IdsupplierNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Idsupplier)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_idsupplier_fkey");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_iduser_fkey");
            });

            modelBuilder.Entity<Cashout>(entity =>
            {
                entity.HasKey(e => e.Idcashout)
                    .HasName("cashout_pkey");

                entity.ToTable("cashout");

                entity.Property(e => e.Idcashout).HasColumnName("idcashout");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Cashoutdate)
                    .HasColumnName("cashoutdate")
                    .HasColumnType("date");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Missing)
                    .HasColumnName("missing")
                    .HasColumnType("money");

                entity.Property(e => e.Sincedate)
                    .HasColumnName("sincedate")
                    .HasColumnType("date");

                entity.Property(e => e.Todate)
                    .HasColumnName("todate")
                    .HasColumnType("date");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Cashout)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cashout_iduser_fkey");
            });

            modelBuilder.Entity<Color>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("color_pkey");

                entity.ToTable("color");

                entity.Property(e => e.Id).HasColumnName("idcolor");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("customer_pkey");

                entity.ToTable("customer");

                entity.Property(e => e.Id).HasColumnName("idcustomer");

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Phonenumber)
                    .IsRequired()
                    .HasColumnName("phonenumber");
            });

            modelBuilder.Entity<Detailcashout>(entity =>
            {
                entity.HasKey(e => new { e.Idcashout, e.Idsale })
                    .HasName("detailcashout_pkey");

                entity.ToTable("detailcashout");

                entity.Property(e => e.Idcashout).HasColumnName("idcashout");

                entity.Property(e => e.Idsale).HasColumnName("idsale");

                entity.HasOne(d => d.IdcashoutNavigation)
                    .WithMany(p => p.Detailcashout)
                    .HasForeignKey(d => d.Idcashout)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailcashout_idcashout_fkey");

                entity.HasOne(d => d.IdsaleNavigation)
                    .WithMany(p => p.Detailcashout)
                    .HasForeignKey(d => d.Idsale)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailcashout_idsale_fkey");
            });

            modelBuilder.Entity<Detailsale>(entity =>
            {
                entity.HasKey(e => new { e.Idsale, e.Idarticle })
                    .HasName("detailsale_pkey");

                entity.ToTable("detailsale");

                entity.Property(e => e.Idsale).HasColumnName("idsale");

                entity.Property(e => e.Idarticle).HasColumnName("idarticle");

                entity.Property(e => e.Additionaldiscount)
                    .HasColumnName("additionaldiscount")
                    .HasColumnType("money");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.IdarticleNavigation)
                    .WithMany(p => p.Detailsale)
                    .HasForeignKey(d => d.Idarticle)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailsale_idarticle_fkey");

                entity.HasOne(d => d.IdsaleNavigation)
                    .WithMany(p => p.Detailsale)
                    .HasForeignKey(d => d.Idsale)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailsale_idsale_fkey");
            });

            modelBuilder.Entity<Detailsalenote>(entity =>
            {
                entity.HasKey(e => new { e.Idsalenote, e.Idarticle })
                    .HasName("detailsalenote_pkey");

                entity.ToTable("detailsalenote");

                entity.Property(e => e.Idsalenote).HasColumnName("idsalenote");

                entity.Property(e => e.Idarticle).HasColumnName("idarticle");

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.HasOne(d => d.IdarticleNavigation)
                    .WithMany(p => p.Detailsalenote)
                    .HasForeignKey(d => d.Idarticle)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailsalenote_idarticle_fkey");

                entity.HasOne(d => d.IdsalenoteNavigation)
                    .WithMany(p => p.Detailsalenote)
                    .HasForeignKey(d => d.Idsalenote)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("detailsalenote_idsalenote_fkey");
            });

            modelBuilder.Entity<Discounttype>(entity =>
            {
                entity.HasKey(e => e.Iddiscounttype)
                    .HasName("discounttype_pkey");

                entity.ToTable("discounttype");

                entity.Property(e => e.Iddiscounttype).HasColumnName("iddiscounttype");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.HasKey(e => e.Idgender)
                    .HasName("gender_pkey");

                entity.ToTable("gender");

                entity.Property(e => e.Idgender).HasColumnName("idgender");

                entity.Property(e => e.Description).HasColumnName("description");
            });

            modelBuilder.Entity<Numeration>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("numeration_pkey");

                entity.ToTable("numeration");

                entity.Property(e => e.Id).HasColumnName("idnumeration");

                entity.Property(e => e.Lowerrange)
                    .HasColumnName("lowerrange")
                    .HasColumnType("numeric(4,2)");

                entity.Property(e => e.Toprange)
                    .HasColumnName("toprange")
                    .HasColumnType("numeric(4,2)");
            });

            modelBuilder.Entity<Payment>(entity =>
            {
                entity.HasKey(e => e.Idpayment)
                    .HasName("payment_pkey");

                entity.ToTable("payment");

                entity.Property(e => e.Idpayment).HasColumnName("idpayment");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Comment).HasColumnName("comment");

                entity.Property(e => e.Idsalenote).HasColumnName("idsalenote");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Paymentdate)
                    .HasColumnName("paymentdate")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdsalenoteNavigation)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.Idsalenote)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("payment_idsalenote_fkey");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Payment)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("payment_iduser_fkey");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("role_pkey");

                entity.ToTable("role");

                entity.Property(e => e.Id).HasColumnName("idrole");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.HasKey(e => e.Idsale)
                    .HasName("sale_pkey");

                entity.ToTable("sale");

                entity.Property(e => e.Idsale).HasColumnName("idsale");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Saledate)
                    .HasColumnName("saledate")
                    .HasColumnType("date");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Sale)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("sale_iduser_fkey");
            });

            modelBuilder.Entity<Salenote>(entity =>
            {
                entity.HasKey(e => e.Idsalenote)
                    .HasName("salenote_pkey");

                entity.ToTable("salenote");

                entity.Property(e => e.Idsalenote).HasColumnName("idsalenote");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.Idcustomer).HasColumnName("idcustomer");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Ispaid).HasColumnName("ispaid");

                entity.Property(e => e.Paymentdate)
                    .HasColumnName("paymentdate")
                    .HasColumnType("date");

                entity.Property(e => e.Saledate)
                    .HasColumnName("saledate")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdcustomerNavigation)
                    .WithMany(p => p.Salenote)
                    .HasForeignKey(d => d.Idcustomer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("salenote_idcustomer_fkey");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Salenote)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("salenote_iduser_fkey");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("supplier_pkey");

                entity.ToTable("supplier");

                entity.Property(e => e.Id).HasColumnName("idsupplier");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Ownername)
                    .IsRequired()
                    .HasColumnName("ownername");

                entity.Property(e => e.Phonenumber)
                    .IsRequired()
                    .HasColumnName("phonenumber");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("users_pkey");

                entity.ToTable("users");

                entity.Property(e => e.Id).HasColumnName("iduser");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Idrole).HasColumnName("idrole");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasColumnType("character(10)");

                entity.HasOne(d => d.IdroleNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.Idrole)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_idrole_fkey");
            });
        }
    }
}
