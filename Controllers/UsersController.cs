using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;
using PointSaleAPI.Models.Request;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly ShoeStoreContext _context;
        private readonly IConfiguration _config;

        public UsersController(ShoeStoreContext context, IConfiguration Configuartion)
        {
            _context = context;
            _config = Configuartion;
        }

        // GET: api/Users
        #region Get Users
        [HttpGet]
        public IEnumerable<UsersDTO> GetUsers()
        {
            return Mapper.Map<IEnumerable<UsersDTO>>(_context.Users.OrderByDescending(x => x.Id));
        }
        #endregion

        // POST: api/Users/Login
        #region Login
        /// <summary>
        /// Description: Login
        /// Developer: Raul Ziranda
        /// Date: 04/09/2019
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.Users.SingleOrDefaultAsync(m =>
                m.Username == credentials.username
                && m.Password == credentials.password
                && m.Active == true);
            
            if (!UserExists(credentials.username)) 
            {
                return NotFound("El usuario no existe");
            }
            else if (user == null)
            {
                return Unauthorized("Contraseña incorrecta");
            }

            var token = GenerateJWT();

            return Ok(new { user = Mapper.Map<UsersDTO>(user), token = token });

        }
        #endregion
        
        // PATCH: api/Users/5
        #region Update User Field
        /// <summary>
        /// Description: Update user field
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchUsers([FromRoute] int id, [FromBody] JsonPatchDocument<Users> updateUser)
        {
            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            updateUser.ApplyTo(user);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok();
        }
        #endregion

        #region Generate JWT
        /// <summary>
        /// Description: Generate JWT
        /// Developer: Raul Ziranda
        /// Date: 04/09/2019
        /// </summary>
        private string GenerateJWT()
        {
            var issuer = _config["JWTManagement:Issuer"];
            var audience = _config["JWTManagement:Audience"];
            var expiry = DateTime.Now.AddMinutes(300);
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWTManagement:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(issuer: issuer,
                audience: audience,
                expires: DateTime.Now.AddMinutes(300),
                signingCredentials: credentials);
            
            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);

            return stringToken;
        }
        #endregion

        #region User Exists
        /// <summary>
        /// Description: Check if User exists by username
        /// Developer: Raul Ziranda
        /// Date: 26/09/2019
        /// <param name="username"></param>
        /// <returns></returns>
        private bool UserExists(string username) => _context.Users.Any(e => e.Username == username);
        #endregion
    }
}