using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ColorsController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public ColorsController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Colors
        #region Get Colors
        /// <summary>
        /// Description: Get all colors
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<ColorDTO> GetColors()
        {
            return Mapper.Map<IEnumerable<ColorDTO>>(_context.Color.OrderByDescending(x => x.Id));
        }
        #endregion

        // GET: api/Colors/5
        #region Get Color By Id
        /// <summary>
        /// Description: Get color by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var color = await _context.Color.SingleOrDefaultAsync(m => m.Id == id);

            if (color == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ColorDTO>(color));
        }
        #endregion
    
        // POST: api/Colors
        #region Add new color
        /// <summary>
        /// Description: Add new color
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostColors([FromBody] ColorDTO color)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newColor = Mapper.Map<Color>(color);

            if (ColorExistsByDescription(color.Description))
            {
                return Conflict("El color ya existe");
            }
            else
            {
                _context.Color.Add(newColor);
                
                try
                {
                    await _context.SaveChangesAsync();
                    color.Id = newColor.Id;
                }
                catch (DbUpdateException)
                {
                    throw;
                }
                
                return CreatedAtAction("GetColors", new { id = newColor.Id }, color);
            }
        }
        #endregion

        // PUT: api/Colors
        #region Edit Color By Id
        /// <summary>
        /// Description: Edit color by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColors([FromRoute] int id,[FromBody] ColorDTO color)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != color.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Color>(color)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!ColorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;    
                }
            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Colors/5
        #region Delete Color By Id
        /// <summary>
        /// Description: Delete color by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var color = await _context.Color.SingleOrDefaultAsync(m => m.Id == id);

            if (color == null)
            {
                return NotFound();
            }

            _context.Color.Remove(color);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<ColorDTO>(color));
        }
        #endregion

        #region Color Exists
        /// <summary>
        /// Description: Check if color exists by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool ColorExists(int id)
        {
            return _context.Color.Any(e => e.Id == id);
        }
        #endregion

        #region Color Exists By Description
        /// <summary>
        /// Description: Check if color exists by description
        /// Developer: Raul Ziranda
        /// Date: 02/10/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool ColorExistsByDescription(string description) => _context.Color.Any(e => e.Description.Equals(description, StringComparison.CurrentCultureIgnoreCase));
        #endregion
    }
}