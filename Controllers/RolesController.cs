using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RolesController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public RolesController(ShoeStoreContext context)
        {
            _context = context;
        }
        
        // GET: api/Roles
        #region Get Roles
        /// <summary>
        /// Description: Get all roles
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<RoleDTO> GetRoles()
        {
            return Mapper.Map<IEnumerable<RoleDTO>>(_context.Role.OrderByDescending(x => x.Id));
        }
        #endregion

        // GET: api/Roles/5
        #region Get Role By Id
        /// <summary>
        /// Description: Get role by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoles([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var role = await _context.Role.SingleOrDefaultAsync(m => m.Id == id);

            if (role == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<RoleDTO>(role));
        }
        #endregion

        // POST: api/Roles
        #region Add new role
        /// <summary>
        /// Description: Add new role
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="role"></parama>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostRoles([FromBody] RoleDTO role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newRole = Mapper.Map<Role>(role);

            _context.Role.Add(newRole);

            try
            {
                await _context.SaveChangesAsync();
                role.Id = newRole.Id;
            }
            catch (DbUpdateException)
            {
                if (RoleExists(newRole.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            
            return CreatedAtAction("GetRoles", new { id = newRole.Id }, role);
        }
        #endregion

        // PUT: api/Roles
        #region Edit Role By Id
        /// <summary>
        /// Description: Edit role by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoles([FromRoute] int id, [FromBody] RoleDTO role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != role.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Role>(role)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!RoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Roles/5
        #region Delete Role By Id
        /// <summary>
        /// Description: Delete role by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRoles([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var role = await _context.Role.SingleOrDefaultAsync(m => m.Id == id);

            if (role == null)
            {
                return NotFound();
            }

            _context.Role.Remove(role);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<RoleDTO>(role));
        }
        #endregion

        #region Role Exists
        /// <summary>
        /// Description: Check if role exists by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool RoleExists(int id)
        {
            return _context.Role.Any(e => e.Id == id);
        }
        #endregion

    }
}