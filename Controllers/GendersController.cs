using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GendersController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public GendersController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Genders
        #region Get Genders
        /// <summary>
        /// Description: Get all Genders
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<GenderDTO> GetGenders()
        {
            return Mapper.Map<IEnumerable<GenderDTO>>(_context.Gender.OrderByDescending(x => x.Idgender));
        }
        #endregion

        // GET: api/Genders/5
        #region Get Gender By Id
        /// <summary>
        /// Description: Get Gender by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGenders([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Gender = await _context.Gender.SingleOrDefaultAsync(m => m.Idgender == id);

            if (Gender == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<GenderDTO>(Gender));
        }
        #endregion
    
        // POST: api/Genders
        #region Add new Gender
        /// <summary>
        /// Description: Add new Gender
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="Gender"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostGenders([FromBody] GenderDTO Gender)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newGender = Mapper.Map<Gender>(Gender);

            _context.Gender.Add(newGender);

            try
            {
                await _context.SaveChangesAsync();
                Gender.Idgender = newGender.Idgender;
            }
            catch (DbUpdateException)
            {
                if (GenderExists(newGender.Idgender))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetGenders", new { id = newGender.Idgender }, Gender);
        }
        #endregion

        // PUT: api/Genders
        #region Edit Gender By Id
        /// <summary>
        /// Description: Edit Gender by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Gender"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGenders([FromRoute] int id,[FromBody] GenderDTO Gender)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Gender.Idgender)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Gender>(Gender)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!GenderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;    
                }
            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Genders/5
        #region Delete Gender By Id
        /// <summary>
        /// Description: Delete Gender by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGenders([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Gender = await _context.Gender.SingleOrDefaultAsync(m => m.Idgender == id);

            if (Gender == null)
            {
                return NotFound();
            }

            _context.Gender.Remove(Gender);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<GenderDTO>(Gender));
        }
        #endregion

        #region Gender Exists
        /// <summary>
        /// Description: Check if Gender exists by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool GenderExists(int id)
        {
            return _context.Gender.Any(e => e.Idgender == id);
        }
        #endregion
    }
}