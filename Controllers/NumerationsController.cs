using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NumerationsController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public NumerationsController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Numerations
        #region Get Numerations
        /// <summary>
        /// Description: Get all Numerations
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<NumerationDTO> GetNumerations() => Mapper.Map<IEnumerable<NumerationDTO>>(_context.Numeration.OrderByDescending(x => x.Id));
        #endregion

        // GET: api/Numerations/5
        #region Get Numeration By Id
        /// <summary>
        /// Description: Get Numeration by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetNumerations([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var numeration = await _context.Numeration.SingleOrDefaultAsync(m => m.Id == id);

            if (numeration == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<NumerationDTO>(numeration));
        }
        #endregion
    
        // POST: api/Numerations
        #region Add new Numeration
        /// <summary>
        /// Description: Add new Numeration
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="Numeration"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostNumerations([FromBody] NumerationDTO numeration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newNumeration = Mapper.Map<Numeration>(numeration);

            _context.Numeration.Add(newNumeration);

            try
            {
                await _context.SaveChangesAsync();
                numeration.Id = newNumeration.Id;
            }
            catch (DbUpdateException)
            {
                if (NumerationExists(newNumeration.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetNumerations", new { id = newNumeration.Id }, numeration);
        }
        #endregion

        // PUT: api/Numerations
        #region Edit Numeration By Id
        /// <summary>
        /// Description: Edit Numeration by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="numeration"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutNumerations([FromRoute] int id,[FromBody] NumerationDTO numeration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != numeration.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Numeration>(numeration)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!NumerationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;    
                }
            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Numerations/5
        #region Delete Numeration By Id
        /// <summary>
        /// Description: Delete Numeration by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNumerations([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var numeration = await _context.Numeration.SingleOrDefaultAsync(m => m.Id == id);

            if (numeration == null)
            {
                return NotFound();
            }

            _context.Numeration.Remove(numeration);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<NumerationDTO>(numeration));
        }
        #endregion

        #region Numeration Exists
        /// <summary>
        /// Description: Check if Numeration exists by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool NumerationExists(int id) => _context.Numeration.Any(e => e.Id == id);
        #endregion
    }
}