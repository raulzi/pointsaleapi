using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SuppliersController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public SuppliersController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Suppliers
        #region Get Suppliers
        /// <summary>
        /// Description: Get all Suppliers
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<SupplierDTO> GetSuppliers() => Mapper.Map<IEnumerable<SupplierDTO>>(_context.Supplier.OrderByDescending(x => x.Id));
        #endregion

        // GET: api/Suppliers/5
        #region Get Supplier By Id
        /// <summary>
        /// Description: Get Supplier by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSuppliers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Supplier = await _context.Supplier.SingleOrDefaultAsync(m => m.Id == id);

            if (Supplier == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<SupplierDTO>(Supplier));
        }
        #endregion
    
        // POST: api/Suppliers
        #region Add new Supplier
        /// <summary>
        /// Description: Add new Supplier
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="Supplier"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostSuppliers([FromBody] SupplierDTO Supplier)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newSupplier = Mapper.Map<Supplier>(Supplier);

            _context.Supplier.Add(newSupplier);

            try
            {
                await _context.SaveChangesAsync();
                Supplier.Id = newSupplier.Id;
            }
            catch (DbUpdateException)
            {
                if (SupplierExists(newSupplier.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSuppliers", new { id = newSupplier.Id }, Supplier);
        }
        #endregion

        // PUT: api/Suppliers
        #region Edit Supplier By Id
        /// <summary>
        /// Description: Edit Supplier by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Supplier"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuppliers([FromRoute] int id,[FromBody] SupplierDTO Supplier)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Supplier.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Supplier>(Supplier)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!SupplierExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;    
                }
            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Suppliers/5
        #region Delete Supplier By Id
        /// <summary>
        /// Description: Delete Supplier by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuppliers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Supplier = await _context.Supplier.SingleOrDefaultAsync(m => m.Id == id);

            if (Supplier == null)
            {
                return NotFound();
            }

            _context.Supplier.Remove(Supplier);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<SupplierDTO>(Supplier));
        }
        #endregion

        #region Supplier Exists
        /// <summary>
        /// Description: Check if Supplier exists by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool SupplierExists(int id) => _context.Supplier.Any(e => e.Id == id);
        #endregion
    }
}