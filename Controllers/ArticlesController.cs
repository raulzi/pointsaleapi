using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ArticlesController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public ArticlesController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Articles
        #region Get Articles
        /// <summary>
        /// Description: Get all articles
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<ArticleDTO> GetArticles()
        {
            return Mapper.Map<IEnumerable<ArticleDTO>>(_context.Article.OrderByDescending(x => x.Id));
        }
        #endregion
        
        // GET: api/Articles/5
        #region Get Article By Id
        /// <summary>
        /// Description: Get article by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticles([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var article = await _context.Article.SingleOrDefaultAsync(m => m.Id == id);

            if (article == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<ArticleDTO>(article));
        }
        #endregion

        #region Article Exists
        /// <summary>
        /// Description: Check if article exists by id
        /// Developer: Raul Ziranda
        /// Date: 05/09/2019
        /// </summary>
        private bool ArticleExists(int id)
        {
            return _context.Article.Any(e => e.Id == id);
        }
        #endregion
    }
}