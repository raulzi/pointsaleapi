using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PointSaleAPI.DTOs;
using PointSaleAPI.Models;

namespace PointSaleAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        private readonly ShoeStoreContext _context;

        public CustomersController(ShoeStoreContext context)
        {
            _context = context;
        }

        // GET: api/Customers
        #region Get Customers
        /// <summary>
        /// Description: Get all Customers
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        [HttpGet]
        public IEnumerable<CustomerDTO> GetCustomers() => Mapper.Map<IEnumerable<CustomerDTO>>(_context.Customer.OrderByDescending(x => x.Id));
        #endregion

        // GET: api/Customers/5
        #region Get Customer By Id
        /// <summary>
        /// Description: Get Customer by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Customer = await _context.Customer.SingleOrDefaultAsync(m => m.Id == id);

            if (Customer == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<CustomerDTO>(Customer));
        }
        #endregion
    
        // POST: api/Customers
        #region Add new Customer
        /// <summary>
        /// Description: Add new Customer
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="Customer"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostCustomers([FromBody] CustomerDTO Customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newCustomer = Mapper.Map<Customer>(Customer);

            _context.Customer.Add(newCustomer);

            try
            {
                await _context.SaveChangesAsync();
                Customer.Id = newCustomer.Id;
            }
            catch (DbUpdateException)
            {
                if (CustomerExists(newCustomer.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCustomers", new { id = newCustomer.Id }, Customer);
        }
        #endregion

        // PUT: api/Customers
        #region Edit Customer By Id
        /// <summary>
        /// Description: Edit Customer by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Customer"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustomers([FromRoute] int id,[FromBody] CustomerDTO Customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Customer.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<Customer>(Customer)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(!CustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;    
                }
            }

            return NoContent();
        }
        #endregion

        // DELETE: api/Customers/5
        #region Delete Customer By Id
        /// <summary>
        /// Description: Delete Customer by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Customer = await _context.Customer.SingleOrDefaultAsync(m => m.Id == id);

            if (Customer == null)
            {
                return NotFound();
            }

            _context.Customer.Remove(Customer);
            await _context.SaveChangesAsync();

            return Ok(Mapper.Map<CustomerDTO>(Customer));
        }
        #endregion

        #region Customer Exists
        /// <summary>
        /// Description: Check if Customer exists by id
        /// Developer: Raul Ziranda
        /// Date: 06/09/2019
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CustomerExists(int id) => _context.Customer.Any(e => e.Id == id);
        #endregion
    }
}